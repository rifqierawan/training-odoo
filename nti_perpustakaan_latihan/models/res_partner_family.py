from odoo import models, fields, api


class ResPartnerFamily(models.Model):
    _name = 'res.partner.family'

    name = fields.Char(string='Name')
    type = fields.Selection([
        ("anak", "Anak"), 
        ("ibu", "Ibu"),
        ("ayah", "Ayah")
    ], string='Type')
