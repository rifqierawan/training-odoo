from odoo import models, fields, api, _
from odoo.exceptions import UserError

class ResParnter(models.Model):
    _inherit = 'res.partner'

    family_ids = fields.Many2many(
        comodel_name='res.partner.family', 
        string='Family'
    )
    