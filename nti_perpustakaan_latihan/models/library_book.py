# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError

class Book(models.Model):
    _name = 'library.book'

    name = fields.Char(string='Name')
    code = fields.Char(string='Code')
    date_of_issue = fields.Date(string='Date of Issue')
    author_ids = fields.Many2many(
        comodel_name='res.partner', 
        string='Author'
    )
    image_1920 = fields.Binary(string='Image')
