from odoo import models, fields, api, _

class HREmployee(models.Model):
    _inherit = 'hr.employee'

    # Tambahkan field relasi one-to-many terhadap model 'hr.certificate' dengan inverse name 'employee_id'
    # certificate_ids

    # Tambahkan field integer dengan compute untuk menghitung jumlah sertifikat
    # certificate_number

    # def _get_certificate_number(self):
    #     for record in self:
    #         record.certificate_number = len(record.certificate_ids)