from odoo import models, fields, api

class HRCertificate(models.Model):
    _name = 'hr.certificate'

    # Tambahkan field berikut :
    # Name - String
    # Type - Selection dengan element (Seminar,Training,Competition,Appreciation)
    # Attachment - M2M (Many-to-Many) terhadap model 'ir.attachment'
    # Issued Date - Date
    # Employee - Many-to-One terhadap hr.employee