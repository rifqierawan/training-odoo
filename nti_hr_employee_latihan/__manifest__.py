# -*- coding: utf-8 -*-
{
    'name': "",

    'summary': """
        Short (1 phrase/line) summary of the module's purpose, used as
        subtitle on modules listing or apps.openerp.com""",

    'description': """
        Long description of module's purpose
    """,
    'author': "",
    'website': "",
    'category': 'Uncategorized',
    'version': '0.1',

    # Tambahkan depend ke module 'base' dan 'hr'
    'depends': [],

    # Daftarkan file view & security
    'data': [],
    'demo': [],
    'application': False
}
